import services.MainServices;

import static services.MainServices.showMenu;
import static services.MainServices.start;

public class Main {
    public static void main(String[] args) {
        start();
        showMenu();
    }
}
