package models;

public enum Mood {
    GOOD(100),
    NORMAL(70),
    BAD(50),
    ILL(5),
    DEATH(0);

   public int health;
    Mood(int i) {
        this.health = i;
    }
}
