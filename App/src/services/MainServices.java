package services;

import models.Mood;

import java.util.Scanner;

public class MainServices {
    static CatLifeStyle cat = new CatLifeStyle();
    static Scanner scanner = new Scanner(System.in);

    public static void start() {
        cat.start();
    }

    public static void showMenu(){

        System.out.println("Menu");
        System.out.println("1-Eat meal");
        System.out.println("2-Drink milk");
        System.out.println("3-Show energy");
        String command = scanner.next();
        switch (command){
            case "1":
                eatMeal();
                showMenu();
                break;
            case "2":
                drinkMilk();
                showMenu();
                break;
            case "3":
                showEnergy();
                showMenu();
                break;
        }
    }


    public static void eatMeal(){

         cat.energy+=9;
      if(cat.energy>=Mood.GOOD.health){
          cat.mood = Mood.GOOD.name();
      }
      else if(cat.energy<Mood.GOOD.health && cat.energy>= Mood.NORMAL.health ){
          cat.mood = Mood.NORMAL.name();
        }
      else if(cat.energy<Mood.NORMAL.health && cat.energy>= Mood.BAD.health){
          cat.mood = Mood.BAD.name();
      }
      else if(cat.energy<Mood.BAD.health && cat.energy>= Mood.ILL.health){
          cat.mood = Mood.ILL.name();
      }
      else if(cat.energy<=Mood.DEATH.health){
          cat.mood = Mood.DEATH.name();
      }
        System.out.println("yedim, enerjim artdi: " + cat.energy);
        System.out.println("moodum: " + cat.mood);
    }

    public static void drinkMilk(){
        cat.energy+=8;
        System.out.println("icdim, enerjim artdi: " + cat.energy);

    }
    public static void showEnergy(){
        System.out.println("energy: " + cat.energy);
    }
}
